package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"path"
	"regexp"
	"syscall"
	"time"

	"bitbucket.org/ildus/noad_bot/telegrambot"
)

var (
	config = flag.String("config", "settings.json", "configuration file")
	conf   Configuration
	bot    *telegrambot.Bot
)

/* Configuration, filled from settings file */
type Configuration struct {
	Listen      string `json:"listen"`
	Token       string `json:"token"`
	BaseDomain  string `json:"base_domain"`
	HookPath    string `json:"hook_path"`
	AdminUserId int    `json:"admin_user_id"`
	Debug       bool   `json:"debug"`
	Database    string `json:"database"`
}

/* Load configuration from specified file */
func loadConfiguration() {
	flag.Parse()
	confData, err := ioutil.ReadFile(*config)
	if err != nil {
		log.Fatal(err)
	}
	err = json.Unmarshal(confData, &conf)
	if err != nil {
		log.Fatal("Configuration decoding error: ", err)
	}

	validToken := regexp.MustCompile(`^\d+:[\w\-]+$`)
	if !validToken.MatchString(conf.Token) {
		log.Fatal("Invalid token format")
	}
	log.Println("Configuration: ok")
}

func listen() {
	var webhookUrl string

	if len(conf.BaseDomain) > 0 && len(conf.HookPath) > 0 && len(conf.Listen) > 0 {
		webhookUrl = "https://" + path.Join(conf.BaseDomain, conf.HookPath)
	} else {
		webhookUrl = ""
	}
	log.Println("Starting the bot")
	bot = telegrambot.MakeBot(webhookUrl, conf.Token, onUpdate, conf.Debug)

	if len(webhookUrl) > 0 {
		routes := mux.NewRouter()
		routes.HandleFunc(conf.HookPath, bot.Hook)
		s := &http.Server{
			Addr:           fmt.Sprintf("%s", conf.Listen),
			Handler:        routes,
			ReadTimeout:    10 * time.Second,
			WriteTimeout:   10 * time.Second,
			MaxHeaderBytes: 1 << 20,
		}

		log.Println("Server started: listening")
		log.Fatal(s.ListenAndServe())
	} else {
		log.Println("Server started: polling")
		for {
			bot.ProcessUpdates()
			time.Sleep(time.Second * 5)
		}
	}
}

func main() {
	go func() {
		sigchan := make(chan os.Signal, 10)
		signal.Notify(sigchan, os.Interrupt, syscall.SIGTERM)
		<-sigchan
		log.Println("Got interrupt signal. Exited")
		os.Exit(0)
	}()

	rand.Seed(time.Now().Unix())
	loadConfiguration()
	if len(conf.Database) > 0 {
		InitDB(conf.Database)
	}
	listen()
}
