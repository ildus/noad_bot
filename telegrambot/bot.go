package telegrambot

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

const (
	BASE_URL = "https://api.telegram.org/bot"
)

type Chat struct {
	Id   int    `json:"id"`
	Type string `json:"type"`
}

type User struct {
	Id        int    `json:"id"`
	IsBot     bool   `json:"is_bot"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Username  string `json:"username"`
}

type MessageEntity struct {
	Type   string `json:"type"`
	Offset int    `json:"offset"`
	Length int    `json:"length"`
	Url    string `json:"url"`
	User   string `json:"user"`
}

type MessageReply struct {
	MessageId int `json:"message_id"`
}

type Photo struct {
	FileId   string `json:"file_id"`
	FileSize int    `json:"file_size"`
	Width    int    `json:"width"`
	Height   int    `json:"height"`
}

type Document struct {
	FileName string `json:"file_name"`
	MimeType string `json:"mime_type"`
}

type Message struct {
	MessageId            int             `json:"message_id"`
	From                 User            `json:"from"`
	Date                 int             `json:"date"`
	Chat                 Chat            `json:"chat"`
	ForwardFrom          User            `json:"forward_from"`
	ForwardFromChat      Chat            `json:"forward_from_chat"`
	ForwardFromMessageId int             `json:"forward_from_message_id"`
	EditDate             int             `json:"edit_date"`
	NewChatMembers       []User          `json:"new_chat_members"`
	Text                 string          `json:"text"`
	Entities             []MessageEntity `json:"entities"`
	CaptionEntities      []MessageEntity `json:"caption_entities"`
	ReplyTo              MessageReply    `json:"reply_to_message"`
	ForwardDate          int             `json:"forward_date"`
	Photos               []Photo         `json:"photo"`
	Document             Document        `json:"document"`
	Sticker              Sticker         `json:"sticker"`
}

type Sticker struct {
	FileId  string `json:"file_id"`
	Width   int    `json:"width"`
	Height  int    `json:"height"`
	Emoji   string `json:"emoji"`
	SetName string `json:"set_name"`
}

type Update struct {
	Id                int     `json:"update_id"`
	Msg               Message `json:"message"`
	EditedMsg         Message `json:"edited_message"`
	ChannelPost       Message `json:"channel_post"`
	EditedChannelPost Message `json:"edited_channel_post"`
}

type Response struct {
	Ok          bool   `json:"ok"`
	Description string `json:"description"`
}

type GenericResponse struct {
	Response
	Result interface{} `json:"result"`
}

type UpdateResponse struct {
	Response
	Updates []Update `json:"result"`
}

type UpdateFunc func(update *Update)
type Bot struct {
	Debug      bool
	Token      string
	OnUpdate   UpdateFunc
	lastOffset int
}

type BotResult interface{}
type ServerResponse struct {
	ok          bool
	description string
	result      BotResult
}

/* Creates bot, checks it and shows in console hook path */
func MakeBot(webhookUrl string, token string, onUpdate UpdateFunc, debug bool) *Bot {
	bot := &Bot{
		Token:      token,
		Debug:      debug,
		OnUpdate:   onUpdate,
		lastOffset: 0,
	}

	ok, info := bot.GetMe()
	if !ok {
		log.Fatal("Bot setup error")
	}

	if bot.Debug {
		log.Printf("Bot username is: %s", info["username"].(string))
	}

	go func() {
		time.Sleep(2 * time.Second)
		ok := bot.SetWebhook(webhookUrl)

		if bot.Debug {
			if ok {
				if len(webhookUrl) > 0 {
					log.Println("Hook expected on: ", webhookUrl)
					log.Println("Bot setWebhook: ok")
				} else {
					log.Println("Bot webhook was removed")
				}
			} else {
				log.Fatal("Bot setWebhook: error")
			}
		}
	}()

	return bot
}

func (bot *Bot) Hook(w http.ResponseWriter, r *http.Request) {
	defer func() {
		if err := recover(); err != nil {
			log.Println("Something is wrong in hook", err)
		}

		//telegram server must not know about our problems
		fmt.Fprintf(w, "OK\n")
	}()
	var update Update
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("Webhook body read error")
		return
	}

	if bot.Debug {
		log.Println(string(body[:]))
	}

	err = json.Unmarshal(body, &update)
	if err != nil {
		log.Println("Webhook json parse error")
		return
	}

	if bot.OnUpdate != nil {
		bot.OnUpdate(&update)
	}
}

func (bot *Bot) MakeRequest(cmd string, params *url.Values) ([]byte, error) {
	var resp *http.Response
	var err error

	//construct url
	cmd_url := BASE_URL + bot.Token + "/" + cmd
	if params == nil {
		resp, err = http.Get(cmd_url)
	} else {
		resp, err = http.PostForm(cmd_url, *params)
	}
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	return body, nil
}

func (bot *Bot) Command(cmd string,
	params *url.Values, result interface{}) *GenericResponse {

	body, err := bot.MakeRequest(cmd, params)
	if err != nil {
		log.Printf("Request error with cmd %s: %s", cmd, err)
		return nil
	}

	if bot.Debug {
		log.Printf("Server response: %s", string(body))
	}

	if result == nil {
		res := &GenericResponse{}
		json.Unmarshal(body, res)
		if !res.Ok {
			log.Printf("Failed request with cmd %s: %s", cmd, res.Description)
			return nil
		}

		return res
	} else {
		err = json.Unmarshal(body, result)
		if err != nil {
			log.Println("Could unmarshal to result", err)
		}
	}
	return nil
}

func (bot *Bot) GetMe() (bool, map[string]interface{}) {
	resp := bot.Command("getMe", nil, nil)
	if resp != nil {
		return true, resp.Result.(map[string]interface{})
	}
	return false, nil
}

func (bot *Bot) ProcessUpdates() bool {
	values := url.Values{}
	if bot.lastOffset > 0 {
		values.Set("offset", strconv.Itoa(bot.lastOffset+1))
	}
	values.Set("timeout", strconv.Itoa(5*60)) // 5 minutes

	result := &UpdateResponse{}
	bot.Command("getUpdates", &values, result)
	if result.Ok {
		for i := range result.Updates {
			update := &result.Updates[i]
			log.Printf("Processing update %d", update.Id)
			bot.OnUpdate(update)
			bot.lastOffset = update.Id
		}
		return true
	}

	return false
}

func (bot *Bot) SendMessage(chat_id int, text string) int {
	values := url.Values{}
	values.Set("chat_id", strconv.Itoa(chat_id))
	values.Set("text", text)
	resp := bot.Command("sendMessage", &values, nil)
	if resp != nil {
		if msg_id, ok := resp.Result.(map[string]interface{})["message_id"]; ok {
			return int(msg_id.(float64))
		}
	}

	return 0
}

func (bot *Bot) ForwardMessage(chat_id int, from_chat_id int,
	disableNotification bool, message_id int) int {

	values := url.Values{}
	values.Set("chat_id", strconv.Itoa(chat_id))
	values.Set("from_chat_id", strconv.Itoa(from_chat_id))
	if disableNotification {
		values.Set("disableNotification", "true")
	} else {
		values.Set("disableNotification", "false")
	}
	values.Set("message_id", strconv.Itoa(message_id))
	resp := bot.Command("forwardMessage", &values, nil)

	if resp != nil {
		if msg_id, ok := resp.Result.(map[string]interface{})["message_id"]; ok {
			return int(msg_id.(float64))
		}
	}

	return 0
}

func (bot *Bot) SendReplyMessage(chat_id int, text string) int {
	values := url.Values{}
	values.Set("chat_id", strconv.Itoa(chat_id))
	values.Set("text", text)
	values.Set("reply_markup", `{"force_reply": true, "selective": false}`)
	resp := bot.Command("sendMessage", &values, nil)
	if resp != nil {
		if msg_id, ok := resp.Result.(map[string]interface{})["message_id"]; ok {
			return int(msg_id.(float64))
		}
	}
	return 0
}

func (bot *Bot) DeleteMessage(chat_id int, message_id int) bool {
	values := url.Values{}
	values.Set("chat_id", strconv.Itoa(chat_id))
	values.Set("message_id", strconv.Itoa(message_id))
	resp := bot.Command("deleteMessage", &values, nil)
	if resp != nil {
		return resp.Result.(bool)
	}
	return false
}

func (bot *Bot) KickChatMember(chat_id int, user_id int, until_date int) bool {
	values := url.Values{}
	values.Set("chat_id", strconv.Itoa(chat_id))
	values.Set("user_id", strconv.Itoa(user_id))
	values.Set("until_date", strconv.Itoa(until_date))
	resp := bot.Command("kickChatMember", &values, nil)
	if resp != nil {
		return resp.Result.(bool)
	}
	return false
}

func (bot *Bot) SetWebhook(hookurl string) bool {
	values := url.Values{}
	values.Set("url", hookurl)
	resp := bot.Command("setWebhook", &values, nil)
	return (resp != nil)
}
