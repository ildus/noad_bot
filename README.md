noad_bot
==============

Prevent spam from new users in telegram chats. If first message of joined user
will have links, photos or documents he will be banned from the chat.

How to run
-----------

Put `settings.json` file along with executable, or specify it in command line
using `-config` parameter.

	{
		"token": "<bot access token>",     #required
		"listen": "127.0.0.1:9000",
		"hook_path": "/bot/hook",
		"base_domain": "example.com",
		"admin_user_id": 0000,
		"debug": true,
		"database": "/db/chats.sqlite"
	}

If any of `listen`, `hook_path` and `base_domain` will not be specified
then bot will use long polling to get updates.

`database` could be used to keep state between restarts if it's important.

Build and run it:

	go build
	./noad_bot -config ./settings.json

If you use webhook also set up proxy in your nginx or another webserver:

    location /bot/hook {
        proxy_set_header host $host;
        proxy_set_header x-real-ip $remote_addr;
        proxy_pass http://localhost:9000;
    }

Set up as docker container.
-------------------------------

* Put these lines to your `docker-compose.yml`:

		noad:
			image: rivitli/noad_bot
			net: host
			ports:
				- "9000:9000"
			volumes:
				- ./settings.json:/settings.json
				- ./db:/db
			restart: always

* Put `settings.json` in same folder.
* Set up your nginx if you use webhook.

How to use
------------

* Create bot in BotFather.
* Set specified access token in `settings.json`.
* Add your bot to your chat or supergroup, and give rights to ban users and
delete messages.
