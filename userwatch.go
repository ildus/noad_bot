package main

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"math/rand"
	"runtime/debug"
	"strconv"
	"strings"

	"bitbucket.org/ildus/noad_bot/telegrambot"
)

const (
	BAN_TIME = 60 * 5
	BOT_NAME = "Noad"
	GREETING = "Hi, my name is " + BOT_NAME
)

var (
	db             *sql.DB = nil
	chats                  = make(map[int]*WatchedChat)
	CHECK_MESSAGES         = [...]string{
		BOT_NAME + " never sleeps",
		"I'm bored, nothing is happening",
		"Oh, hey",
		"Sup",
		"What's up!",
		"I'm ok"}
)

/* Contains user details */
type WatchedUser struct {
	Chat       int `json:"chat_id"`
	JoinedDate int `json:"joined_date"`
	User       int `json:"user_id"`
}

type WatchedChat struct {
	id               int
	users            map[int]*WatchedUser `json:"users"`
	blocked_stickers []string
	blocked_images   []string
}

func messageHasLinks(message *telegrambot.Message) bool {
	for i := range message.Entities {
		entity := &message.Entities[i]
		if len(entity.Url) > 0 {
			return true
		}

		if entity.Type == "text_link" {
			return true
		}

		if entity.Type == "url" {
			return true
		}
	}
	return false
}

func messageHasPhotos(message *telegrambot.Message) bool {
	return len(message.Photos) > 0
}

func messageHasDocuments(message *telegrambot.Message) bool {
	return len(message.Document.FileName) > 0
}

func messageWasForwarded(message *telegrambot.Message) bool {
	return message.ForwardFromChat.Id > 0
}

func MessageShouldBeRemoved(message *telegrambot.Message) string {
	if messageHasLinks(message) {
		return "has links"
	}

	if messageHasPhotos(message) {
		return "has photos"
	}

	if messageHasDocuments(message) {
		return "has documents"
	}

	if messageWasForwarded(message) {
		return "was forwarded"
	}

	return ""
}

func GetWatchedChat(chat_id int) *WatchedChat {
	var wchat *WatchedChat
	var ok bool

	if wchat, ok = chats[chat_id]; !ok {
		wchat = &WatchedChat{id: chat_id, users: make(map[int]*WatchedUser)}
		chats[chat_id] = wchat
	}

	return wchat
}

func InitDB(connstring string) {
	var err error

	db, err = sql.Open("sqlite3", connstring)
	if err != nil {
		log.Fatal("Could not open database ", err)
	}

	_, err = db.Exec(`create table if not exists watched_users(chat_id int,
		joined_date int, user_id int);`)
	_, err = db.Exec(`create table if not exists blocked_stickers(chat_id int,
		sticker_setname text);`)
	_, err = db.Exec(`create table if not exists blocked_images(chat_id int,
		file_id text);`)

	if err != nil {
		log.Println("could not create table for watched users")
	}

	// restore watched users
	rows, err := db.Query("select chat_id, joined_date, user_id from watched_users")
	for rows.Next() {
		wuser := &WatchedUser{}
		err = rows.Scan(&wuser.Chat, &wuser.JoinedDate, &wuser.User)
		if err != nil {
			log.Fatal("Could not fetch watched user:", err)
		}
		wchat := GetWatchedChat(wuser.Chat)
		wchat.users[wuser.User] = wuser
		log.Printf("User %d added to watch list", wuser.User)
	}
	rows.Close()

	// restore blocked stickers
	rows, err = db.Query("select chat_id, sticker_setname from blocked_stickers")
	for rows.Next() {
		var chat_id int
		var setname string

		err = rows.Scan(&chat_id, &setname)
		if err != nil {
			log.Fatal("Could not fetch blocked sticker set name:", err)
		}
		wchat := GetWatchedChat(chat_id)
		wchat.blocked_stickers = append(wchat.blocked_stickers, setname)
		log.Printf("Sticker set %s added to blocked list of %d",
			setname, chat_id)
	}
	rows.Close()

	// restore blocked images
	rows, err = db.Query("select chat_id, file_id from blocked_images")
	for rows.Next() {
		var chat_id int
		var file_id string

		err = rows.Scan(&chat_id, &file_id)
		if err != nil {
			log.Fatal("Could not fetch blocked sticker set name:", err)
		}
		wchat := GetWatchedChat(chat_id)
		wchat.blocked_images = append(wchat.blocked_images, file_id)
		log.Printf("image %s added to blocked images of %d",
			file_id, chat_id)
	}
	rows.Close()

	log.Println("Database initialized")
}

func StoreWatchedUser(wchat *WatchedChat, wuser *WatchedUser) {
	wchat.users[wuser.User] = wuser
	_, err := db.Exec("insert into watched_users values (?,?,?)",
		wuser.Chat, wuser.JoinedDate, wuser.User)

	if err != nil {
		log.Fatal("Could not store watched user: ", err)
	}
	log.Printf("User %d added to watch list", wuser.User)
}

func DeleteWatchedUser(wchat *WatchedChat, wuser *WatchedUser) {
	delete(wchat.users, wuser.User)

	_, err := db.Exec("delete from watched_users where chat_id=? and user_id=?",
		wuser.Chat, wuser.User)
	if err != nil {
		log.Fatal("Could not remove watched user: ", err)
	}

	log.Printf("User %d was removed from watch list", wuser.User)
}

func StoreWatchedStickerSet(wchat *WatchedChat, setname string) {
	pos := -1
	for i := range wchat.blocked_stickers {
		if wchat.blocked_stickers[i] == setname {
			pos = i
			break
		}
	}

	if pos == -1 {
		// add
		wchat.blocked_stickers = append(wchat.blocked_stickers, setname)

		_, err := db.Exec("insert into blocked_stickers values (?,?)",
			wchat.id, setname)

		if err != nil {
			log.Fatal("Could not store blocked sticker set: ", err)
		}
		notifyAdmin(fmt.Sprintf("Sticker set %s added to blocked list of %d",
			setname, wchat.id))
	} else {
		// remove
		wchat.blocked_stickers = append(wchat.blocked_stickers[:pos],
			wchat.blocked_stickers[pos+1:]...)

		_, err := db.Exec(`delete from blocked_stickers where chat_id=?
							and sticker_setname=?`,
			wchat.id, setname)

		if err != nil {
			log.Fatal("Could not remove blocker sticker set: ", err)
		}
		notifyAdmin(fmt.Sprintf("Sticker set %s removed from blocked list of %d",
			setname, wchat.id))
	}
}

func StoreBlockedImage(wchat *WatchedChat, file_id string) {
	pos := -1
	for i := range wchat.blocked_images {
		if wchat.blocked_images[i] == file_id {
			pos = i
			break
		}
	}

	if pos == -1 {
		// add
		wchat.blocked_images = append(wchat.blocked_images, file_id)

		_, err := db.Exec("insert into blocked_images values (?,?)",
			wchat.id, file_id)

		if err != nil {
			log.Fatal("Could not store blocked image: ", err)
		}
		notifyAdmin(fmt.Sprintf("image %s added to blocked images of %d",
			file_id, wchat.id))
	} else {
		// remove
		wchat.blocked_images = append(wchat.blocked_images[:pos],
			wchat.blocked_images[pos+1:]...)

		_, err := db.Exec(`delete from blocked_images where chat_id=?
							and file_id=?`,
			wchat.id, file_id)

		if err != nil {
			log.Fatal("Could not remove blocker image: ", err)
		}
		notifyAdmin(fmt.Sprintf("image %s removed from blocked images of %d",
			file_id, wchat.id))
	}
}

func notifyAdmin(msg string) {
	if conf.AdminUserId > 0 {
		bot.SendMessage(conf.AdminUserId, msg)
	}
}

func onUpdate(update *telegrambot.Update) {
	var wchat *WatchedChat

	defer func() {
		if err := recover(); err != nil {
			log.Println("Update handling error: ", err)

			if conf.Debug {
				debug.PrintStack()
			}
		}
	}()

	// only handle messages
	var message *telegrambot.Message

	if update.Msg.MessageId != 0 {
		message = &update.Msg
	} else if update.EditedMsg.MessageId != 0 {
		message = &update.EditedMsg
	} else {
		return
	}

	var userFrom *telegrambot.User = &message.From

	if userFrom.Id == 0 {
		return
	}

	if message.Chat.Type == "private" {
		//handle commands only in private messages
		for i := range message.Entities {
			entity := &message.Entities[i]
			if entity.Type == "bot_command" {
				var parts []string = nil

				command := message.Text[entity.Offset:entity.Length]
				if userFrom.Id == conf.AdminUserId {
					pos1 := entity.Offset + entity.Length + 1
					pos2 := len(message.Text)
					if pos1 < pos2 {
						tail := message.Text[pos1:pos2]
						parts = strings.Split(tail, " ")
					}
				}

				switch command {
				case "/start":
					bot.SendMessage(message.Chat.Id, GREETING)
				case "/check":
					bot.SendMessage(message.Chat.Id,
						CHECK_MESSAGES[rand.Intn(len(CHECK_MESSAGES))])
				}

				var copy []string
				for i := range parts {
					item := strings.Trim(parts[i], " ")
					if len(item) > 0 {
						copy = append(copy, item)
					}
				}

				parts = copy
				if parts != nil && len(parts) >= 2 {
					chatId, err := strconv.Atoi(parts[0])
					if err == nil {
						switch command {
						case "/report":
							messageId, err := strconv.Atoi(parts[1])
							if err == nil {
								bot.DeleteMessage(chatId, messageId)
								log.Printf("removed reported message %d from chat %d",
									messageId, chatId)
							} else {
								log.Println("could not parse message id: ", messageId)
							}
						case "/stickers":
							wchat = GetWatchedChat(chatId)
							StoreWatchedStickerSet(wchat, parts[1])
						case "/images":
							wchat = GetWatchedChat(chatId)
							StoreBlockedImage(wchat, parts[1])
						}
					} else {
						log.Println("could not parse chat id: ", err)
					}
				}
			}
		}

		//handle stickers to block
		if message.ForwardFromChat.Id > 0 && len(message.Sticker.FileId) > 0 {
			wchat = GetWatchedChat(message.ForwardFromChat.Id)
			StoreWatchedStickerSet(wchat, message.Sticker.SetName)
		}
	} else {
		var wuser *WatchedUser
		var ok bool

		// we're checking only chat messages
		if message.Chat.Type != "group" && message.Chat.Type != "supergroup" {
			return
		}

		wchat = GetWatchedChat(message.Chat.Id)

		if len(message.NewChatMembers) > 0 {
			for i := range message.NewChatMembers {
				member := &message.NewChatMembers[i]
				wuser = &WatchedUser{
					Chat:       message.Chat.Id,
					JoinedDate: message.Date,
					User:       member.Id,
				}
				StoreWatchedUser(wchat, wuser)
			}
		} else {
			// check for watched users first
			if wuser, ok = wchat.users[userFrom.Id]; ok {
				reason := MessageShouldBeRemoved(message)

				if len(reason) > 0 {
					if conf.Debug && conf.AdminUserId > 0 {
						bot.ForwardMessage(conf.AdminUserId, message.Chat.Id,
							true, message.MessageId)
					}

					if bot.DeleteMessage(message.Chat.Id, message.MessageId) {
						bot.KickChatMember(message.Chat.Id, userFrom.Id,
							message.Date+BAN_TIME)

						// also send message to admin about removed message
						if conf.Debug && conf.AdminUserId > 0 {
							bot.SendMessage(conf.AdminUserId,
								fmt.Sprintf(
									"banned user %d from chat %d because message %s",
									userFrom.Id, message.Chat.Id, reason))
						}
					}
				}
				DeleteWatchedUser(wchat, wuser)
			}

			// now check for unlegal stickers
			if len(message.Sticker.FileId) > 0 {
				for i := range wchat.blocked_stickers {
					setname := wchat.blocked_stickers[i]
					if setname == message.Sticker.SetName {
						bot.DeleteMessage(message.Chat.Id, message.MessageId)
					}
				}
			}

			// now check for unlegal images
			if len(message.Photos) > 0 {
				for j := range message.Photos {
					photo := &message.Photos[j]

					for i := range wchat.blocked_images {
						file_id := wchat.blocked_images[i]
						if file_id == photo.FileId {
							bot.DeleteMessage(message.Chat.Id, message.MessageId)
							break
						}
					}
				}
			}

			//go on
		}
	}
}
